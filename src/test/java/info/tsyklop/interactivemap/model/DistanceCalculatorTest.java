package info.tsyklop.interactivemap.model;

import org.junit.Test;

import static org.junit.jupiter.api.Assertions.assertEquals;

public class DistanceCalculatorTest {

    @Test
    public void distance() {

        double lat1 = 32.9697;
        double lat2 = 29.46786;
        double lon1 = -96.80322;
        double lon2 = -98.53506;

        assertEquals(422.7592707099521, new DistanceCalculator().distance(lat1, lon1, lat2, lon2));

    }

}