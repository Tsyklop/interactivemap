package info.tsyklop.interactivemap.pojo;

import lombok.Builder;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.experimental.SuperBuilder;

import java.util.List;

@Data
@SuperBuilder
@EqualsAndHashCode(callSuper = true)
public class PropertiesDistancePageDTO extends PropertiesPageDTO {

    private Double distance;

}
