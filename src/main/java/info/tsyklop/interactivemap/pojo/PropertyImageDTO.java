package info.tsyklop.interactivemap.pojo;

import lombok.Data;
import lombok.EqualsAndHashCode;

import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.FetchType;
import javax.persistence.Lob;
import java.sql.Blob;

@Data
@EqualsAndHashCode(callSuper = true)
public class PropertyImageDTO extends AbstractDTO {

    private byte[] image;

}
