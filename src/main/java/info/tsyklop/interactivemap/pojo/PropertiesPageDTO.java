package info.tsyklop.interactivemap.pojo;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.experimental.SuperBuilder;

import java.util.List;

@Data
@SuperBuilder
@NoArgsConstructor
@AllArgsConstructor
public class PropertiesPageDTO {

    private Integer page;

    private Integer count;

    private Integer totalPages;

    private List<PropertyDTO> properties;

}
