package info.tsyklop.interactivemap.pojo;

import lombok.Data;
import lombok.EqualsAndHashCode;

@Data
@EqualsAndHashCode(callSuper = true)
public class PropertyDTO extends AbstractDTO {

    private Double x;
    private Double y;

    private String address;

    private String description;

    private Integer roomsCount;

    private Integer quadrature;

}
