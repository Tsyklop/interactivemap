package info.tsyklop.interactivemap.pojo;

import lombok.Data;
import lombok.EqualsAndHashCode;

import java.util.Set;

@Data
@EqualsAndHashCode(callSuper = true)
public class PropertyExtendedDTO extends PropertyDTO {

    private Set<PropertyImageDTO> images;

}
