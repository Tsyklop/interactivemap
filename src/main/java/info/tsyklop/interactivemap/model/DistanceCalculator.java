package info.tsyklop.interactivemap.model;

import org.springframework.stereotype.Component;

@Component
public class DistanceCalculator {

    private static final Double EARTH_RADIUS = 6371D;

    public double distance(double lat1, double lon1, double lat2, double lon2) {

        lon1 = Math.toRadians(lon1);
        lon2 = Math.toRadians(lon2);
        lat1 = Math.toRadians(lat1);
        lat2 = Math.toRadians(lat2);

        double a = Math.pow(Math.sin((lat2 - lat1) / 2), 2)
                + Math.cos(lat1) * Math.cos(lat2)
                * Math.pow(Math.sin((lon2 - lon1) / 2),2);

        return (2 * Math.asin(Math.sqrt(a))) * EARTH_RADIUS;

    }

}
