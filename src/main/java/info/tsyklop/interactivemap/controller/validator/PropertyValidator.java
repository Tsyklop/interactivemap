package info.tsyklop.interactivemap.controller.validator;

import info.tsyklop.interactivemap.pojo.PropertyDTO;
import org.springframework.stereotype.Component;
import org.springframework.validation.Errors;
import org.springframework.validation.ValidationUtils;
import org.springframework.validation.Validator;

import java.util.Objects;

@Component
public class PropertyValidator implements Validator {

    @Override
    public boolean supports(Class<?> clazz) {
        return PropertyDTO.class.equals(clazz);
    }

    @Override
    public void validate(Object target, Errors errors) {

        ValidationUtils.rejectIfEmptyOrWhitespace(errors, "address", "address.empty", "Address Incorrect");
        ValidationUtils.rejectIfEmptyOrWhitespace(errors, "description", "description.empty", "Description Incorrect");

        PropertyDTO p = (PropertyDTO) target;

        if (Objects.isNull(p.getX())) {
            errors.rejectValue("x", "coordinate.null", "X coordinate cannot be null");
        } else if (Objects.isNull(p.getY())) {
            errors.rejectValue("y", "coordinate.null", "Y coordinate cannot be null");
        }

    }

}
