package info.tsyklop.interactivemap.controller;

import info.tsyklop.interactivemap.controller.validator.PropertyValidator;
import info.tsyklop.interactivemap.exception.PropertyException;
import info.tsyklop.interactivemap.pojo.PropertyDTO;
import info.tsyklop.interactivemap.service.PropertyService;
import lombok.RequiredArgsConstructor;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.BindingResult;
import org.springframework.validation.Errors;
import org.springframework.validation.Validator;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.WebDataBinder;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;

import javax.validation.Valid;

@RestController
@RequestMapping(value = "/api/v1/property", consumes = MediaType.APPLICATION_JSON_VALUE, produces = MediaType.APPLICATION_JSON_VALUE)
@RequiredArgsConstructor
public class PropertyRestController extends AbstractRestController {

    private final PropertyService propertyService;

    @InitBinder
    protected void initBinder(WebDataBinder binder) {
        binder.setValidator(new PropertyValidator());
    }

    @GetMapping
    public ResponseEntity findAll(@RequestParam(value = "page", defaultValue = "0") Integer page) {
        return this.propertyService.findAll(page);
    }

    @GetMapping("/{x}/{y}")
    public ResponseEntity findAllByDistance(@PathVariable Double x, @PathVariable Double y,
                                            @RequestParam(name = "dist", defaultValue = "10") Double dist,
                                            @RequestParam(name = "page", defaultValue = "0") Integer page) {
        return this.propertyService.findAllByDistance(x, y, dist, page);
    }

    @GetMapping("/{id}")
    public ResponseEntity findById(@PathVariable Long id) {
        return this.propertyService.findById(id);
    }

    @PostMapping(consumes = MediaType.MULTIPART_FORM_DATA_VALUE)
    public ResponseEntity save(@Valid PropertyDTO property, @RequestParam("images") MultipartFile[] images, Errors errors) {
        if(errors.hasErrors()) {
            return ResponseEntity.badRequest().body(errors.getAllErrors());
        } else {
            return this.propertyService.save(property, images);
        }
    }

    @PutMapping("/{id}")
    public ResponseEntity update(@PathVariable Long id, @RequestBody PropertyDTO property) {
        return this.propertyService.update(id, property);
    }

    @DeleteMapping("/{id}")
    public ResponseEntity deleteById(@PathVariable Long id) {
        return this.propertyService.deleteById(id);
    }

    @ExceptionHandler(PropertyException.class)
    public ResponseEntity handlePropertyException(PropertyException e) {
        return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).body(e.getMessage());
    }

}
