package info.tsyklop.interactivemap.persistence.repository;

import info.tsyklop.interactivemap.persistence.entity.PropertyImageEntity;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface PropertyImageRepository extends JpaRepository<PropertyImageEntity, Long> {
}
