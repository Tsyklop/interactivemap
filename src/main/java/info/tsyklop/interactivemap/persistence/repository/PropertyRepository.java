package info.tsyklop.interactivemap.persistence.repository;

import info.tsyklop.interactivemap.persistence.entity.PropertyEntity;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

@Repository
public interface PropertyRepository extends JpaRepository<PropertyEntity, Long> {

    boolean existsByAddressOrXAndY(String address, Double x, Double y);

    @Query(value = "SELECT * FROM `property` AS p " +
            "WHERE (2 * 6371) * asin(sqrt(power(sin(radians((?1 - p.x) / 2)), 2) + cos(radians(p.x)) * cos(radians(?1)) * power(sin(radians((?2 - p.y) / 2)), 2))) <= ?3",
            countQuery = "SELECT COUNT(*) FROM `property` AS p " +
                    "WHERE (2 * 6371) * asin(sqrt(power(sin(radians((?1 - p.x) / 2)), 2) + cos(radians(p.x)) * cos(radians(?1)) * power(sin(radians((?2 - p.y) / 2)), 2))) <= ?3",
            nativeQuery = true)
    Page<PropertyEntity> findAllByDistance(Double x, Double y, Double distance, Pageable pageable);

}
