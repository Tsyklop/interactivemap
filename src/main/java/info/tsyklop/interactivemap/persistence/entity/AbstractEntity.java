package info.tsyklop.interactivemap.persistence.entity;

import lombok.Data;

import javax.persistence.*;
import java.time.LocalDateTime;

@Data
@MappedSuperclass
public abstract class AbstractEntity {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    private LocalDateTime updated;

    private LocalDateTime created;

    @PreUpdate
    public void onUpdate() {
        this.updated = LocalDateTime.now();
    }

    @PrePersist
    public void onCreate() {
        this.updated = LocalDateTime.now();
        this.created = LocalDateTime.now();
    }

}
