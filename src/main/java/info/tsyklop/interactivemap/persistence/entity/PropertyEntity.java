package info.tsyklop.interactivemap.persistence.entity;

import lombok.*;

import javax.persistence.Entity;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import java.util.HashSet;
import java.util.Set;

@Data
@Table(name = "property")
@Entity
@Builder
@EqualsAndHashCode(callSuper = true)
@NoArgsConstructor
@AllArgsConstructor
public class PropertyEntity extends AbstractEntity {

    private Double x;
    private Double y;

    private String address;

    private String description;

    private Integer roomsCount;

    private Integer quadrature;

    @Builder.Default
    @OneToMany(mappedBy="property")
    @EqualsAndHashCode.Exclude
    private Set<PropertyImageEntity> images = new HashSet<>();

}
