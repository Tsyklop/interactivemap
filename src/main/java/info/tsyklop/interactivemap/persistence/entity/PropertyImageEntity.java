package info.tsyklop.interactivemap.persistence.entity;

import lombok.*;

import javax.persistence.*;

@Data
@Table(name = "propertyImage")
@Entity
@Builder
@EqualsAndHashCode(callSuper = true)
@NoArgsConstructor
@AllArgsConstructor
public class PropertyImageEntity extends AbstractEntity {

    @Lob
    @Basic(fetch = FetchType.EAGER)
    @Column(name = "image", columnDefinition="BLOB")
    private byte[] image;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name="propertyId", nullable=false)
    private PropertyEntity property;

}
