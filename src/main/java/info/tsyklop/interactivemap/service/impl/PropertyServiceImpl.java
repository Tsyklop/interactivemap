package info.tsyklop.interactivemap.service.impl;

import info.tsyklop.interactivemap.exception.PropertyException;
import info.tsyklop.interactivemap.persistence.entity.PropertyEntity;
import info.tsyklop.interactivemap.persistence.entity.PropertyImageEntity;
import info.tsyklop.interactivemap.persistence.repository.PropertyImageRepository;
import info.tsyklop.interactivemap.persistence.repository.PropertyRepository;
import info.tsyklop.interactivemap.pojo.PropertiesDistancePageDTO;
import info.tsyklop.interactivemap.pojo.PropertiesPageDTO;
import info.tsyklop.interactivemap.pojo.PropertyDTO;
import info.tsyklop.interactivemap.pojo.PropertyExtendedDTO;
import info.tsyklop.interactivemap.service.PropertyService;
import lombok.RequiredArgsConstructor;
import org.hibernate.exception.DataException;
import org.modelmapper.ModelMapper;
import org.modelmapper.TypeToken;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.multipart.MultipartFile;

import java.io.IOException;
import java.util.*;

@Service
@RequiredArgsConstructor
public class PropertyServiceImpl implements PropertyService {

    private final ModelMapper modelMapper;

    private final PropertyRepository propertyRepository;
    private final PropertyImageRepository propertyImageRepository;

    @Override
    @Transactional(readOnly = true)
    public ResponseEntity findAll(Integer pageNumber) {

        Page<PropertyEntity> propertiesPage = propertyRepository.findAll(PageRequest.of(pageNumber, 10));

        return ResponseEntity.ok(PropertiesPageDTO.builder()
                .page(pageNumber)
                .count(propertiesPage.getNumberOfElements())
                .totalPages(propertiesPage.getTotalPages())
                .properties(modelMapper.map(propertiesPage.getContent(), new TypeToken<List<PropertyExtendedDTO>>() {
                }.getType())).build());
    }

    @Override
    @Transactional(readOnly = true)
    public ResponseEntity findAllByDistance(Double x, Double y, Double dist, Integer pageNumber) {

        Page<PropertyEntity> propertiesPage = this.propertyRepository.findAllByDistance(x, y, dist, PageRequest.of(pageNumber, 10));

        PropertiesDistancePageDTO propertiesPageDTO = PropertiesDistancePageDTO.builder()
                .distance(dist)
                .page(pageNumber)
                .count(propertiesPage.getNumberOfElements())
                .totalPages(propertiesPage.getTotalPages())
                .properties(modelMapper.map(propertiesPage.getContent(), new TypeToken<List<PropertyExtendedDTO>>() {}.getType()))
                .build();

        return ResponseEntity.ok(propertiesPageDTO);

    }

    @Override
    @Transactional(readOnly = true)
    public ResponseEntity findById(Long id) {

        Optional<PropertyEntity> propertyOptional = propertyRepository.findById(id);

        if (!propertyOptional.isPresent()) {
            return ResponseEntity.status(HttpStatus.NOT_FOUND).body("Property not found");
        }

        return ResponseEntity.ok(modelMapper.map(propertyOptional.get(), PropertyExtendedDTO.class));
    }

    @Override
    @Transactional
    public ResponseEntity save(PropertyDTO property, MultipartFile[] images) {

        if (this.propertyRepository.existsByAddressOrXAndY(property.getAddress(), property.getX(), property.getY())) {
            return ResponseEntity.badRequest().body("This property already exists");
        }

        if(Objects.isNull(images) || images.length < 1) {
            return ResponseEntity.badRequest().body("Please, attach minimum 1 property image");
        }

        PropertyEntity propertyEntity = PropertyEntity.builder()
                .x(property.getX())
                .y(property.getY())
                .address(property.getAddress())
                .quadrature(property.getQuadrature())
                .roomsCount(property.getRoomsCount())
                .description(property.getDescription())
                .build();

        this.propertyRepository.save(propertyEntity);

        Set<PropertyImageEntity> propertyImages = new HashSet<>();

        for(MultipartFile image: images) {

            try {

                PropertyImageEntity propertyImageEntity = PropertyImageEntity.builder().image(image.getBytes()).property(propertyEntity).build();
                this.propertyImageRepository.save(propertyImageEntity);
                propertyImages.add(propertyImageEntity);

            } catch (IOException e) {
                throw new PropertyException("Error when saving image: ", e);
            }

        }

        propertyEntity.setImages(propertyImages);

        this.propertyRepository.save(propertyEntity);

        return ResponseEntity.status(HttpStatus.CREATED).body("Property successfully created");

    }

    @Override
    public ResponseEntity update(Long id, PropertyDTO property) {
        return null;
    }

    @Override
    public ResponseEntity deleteById(Long id) {

        if (!this.propertyRepository.existsById(id)) {
            return ResponseEntity.notFound().build();
        }

        this.propertyRepository.deleteById(id);

        return ResponseEntity.ok("Property successfully deleted");

    }

}
