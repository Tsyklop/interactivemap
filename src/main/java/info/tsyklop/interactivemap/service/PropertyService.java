package info.tsyklop.interactivemap.service;

import info.tsyklop.interactivemap.pojo.PropertyDTO;
import org.springframework.http.ResponseEntity;
import org.springframework.web.multipart.MultipartFile;

import java.io.IOException;

public interface PropertyService {

    ResponseEntity findAll(Integer pageNumber);
    ResponseEntity findAllByDistance(Double x, Double y, Double dist, Integer pageNumber);

    ResponseEntity findById(Long id);

    ResponseEntity save(PropertyDTO property, MultipartFile[] images);

    ResponseEntity update(Long id, PropertyDTO property);

    ResponseEntity deleteById(Long id);

}
